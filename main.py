import os
from gitlab import Gitlab

PRIVATE_TOKEN = os.environ["GITLAB_TOKEN"]
GITLAB_PROJECT_ID = ["CI_PROJECT_ID"]
MR_ID = os.environ["CI_MERGE_REQUEST_IID"]

GITLAB_URL = "https://gitlab.com"


if __name__ == "__main__":
    gl = Gitlab(GITLAB_URL, private_token=PRIVATE_TOKEN)
    gl.auth()
    project = gl.projects.get(GITLAB_PROJECT_ID)
    print(project.mergerequests.get(MR_ID))
